#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Connects to OmeroSqlDB
Queries for ontology terms annotated projects
Retreives information needed for GAF2 files creation
Uses files: database.ini, c_elegans...GeneIDs.txt, ontologyTermsIds.txt
Creates files: sqlExtract.csv, sqlExtractAsGaf.wb
@author: Christophe HELIGON
Date: 08June2020
"""

import psycopg2
from configparser import ConfigParser
import re


def config(filename='database.ini', section='postgresql'):
    """Extracts config information for postgresql db connection"""
    # creates a parser
    parser = ConfigParser()
    # reads config file
    parser.read(filename)
    
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))
    return db

            
def geneList(query):
    """Connects to database, passes a query and returns the result"""
    conn = None
    result = None
    try:
        # reads connection parameters and connects to the DB
        params = config()
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)
        
        # creates a cursor, executes a query and gets the answer
        cur = conn.cursor()
        print("Submits query...")
        cur.execute(query)
        result = cur.fetchall()
        print("Result stored")
        cur.close()
    
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')
    return result

def dictGeneId(geneIdFileName):
    """Generates a dictionnary from the wormbase gene_id file
    linking gene name to geneID"""
    with open (geneIdFileName,"r") as geneIdFile:
        geneIdsDict = {}
        for line in geneIdFile:
            line = line.rstrip()
            line = re.split(",",line)
            geneIdsDict[line[2]] = line[1]
        return geneIdsDict

def dictTypeId(geneIdFileName):
    """Generates a dictionnary from the wormbase gene_id file
    linking gene name to object Type"""
    with open (geneIdFileName,"r") as geneIdFile:
        TypeDict = {}
        for line in geneIdFile:
            line = line.rstrip()
            line = re.split(",",line)
            TypeDict[line[2]] = line[5]
        return TypeDict
    
def dictSynonym(geneIdFileName):
    """Generates a dictionnary from the wormbase gene_id file
    linking gene name to object Type"""
    with open (geneIdFileName,"r") as geneIdFile:
        SynonymDict = {}
        for line in geneIdFile:
            line = line.rstrip()
            line = re.split(",",line)
            SynonymDict[line[2]] = line[3]
        return SynonymDict
    
def queryResultToCsv(qresult):
    """converts a list to csv"""
    csv = ""
    for element in qresult:
        line = ""
        for info in element:
            line = line + "," + str(info)
        csv = csv + line[1:len(line)] + "\n"
    return csv

def csvToGAF2(filename, ontoDict):
    """takes the csv formated query response file and converts it to a
    GAF2 file format (17 tab separated colomns per line) """
    
    with open(sqlExtract, "r") as sqlresult:
        gaf = "!gaf-version: 2.0\n"
        gaf = gaf + "!Project_name: CEDRE\n"
        gaf = gaf + "!Contact Email: christophe.heligon@univ-rennes1.fr\n"
        for line in sqlresult:
            line = line.rstrip()
            line= re.split(",",line)
            gafline = "WB"                                          #1 db id
            gafline = gafline + "\t" + geneId[line[3]]              #2 db object id
            gafline = gafline + "\t" + line[3]                      #3 db object symbol
            gafline = gafline + "\t" + ""                           #4 OPT / qualifier (NOT)
            gafline = gafline + "\t" + ontoDict[line[5]]            #5 GO ID G0:0000000 TODO
            gafline = gafline + "\t" + "CedreProject:" + line[0]    #6 DB:Reference PMID:0000000
            gafline = gafline + "\t" + "IMP"                        #7 IMP is for mutant
            gafline = gafline + "\t"                                #8
            if "RNAi" in line[2]:                                   #8
                gafline = gafline + "CedreRNAi:tbd|"                #8
            gafline = gafline + "eco:0007042"                       #8 From evidence code ontology
            gafline = gafline + "\t" + "P"                          #9 Aspect P (biological process), F (molecular function) or C (cellular component)
            gafline = gafline + "\t" + ""                           #10 OPT / DB object name toll like receptor 4
            gafline = gafline + "\t" + Synonym[line[3]]             #11 OPT / DB object synonym
            gafline = gafline + "\t" + TypeCode[line[3]]            #12 DB object type
            gafline = gafline + "\t" + "taxon:6239"                 #13 Taxon = taxon:
            gafline = gafline + "\t" + line[6].replace("-","")      #14 Date 20200618
            gafline = gafline + "\t" + "http://www.igdr.fr/cedre/"  #15 Assigned by
            gafline = gafline + "\t" + ""                           #16 OPT / Annotation extension
            gafline = gafline + "\t" + ""                           #17 OPT / Gene Product form ID
            gaf = gaf + gafline + "\n"
    with open("sqlExtractAsGaf.wb","w") as gaffile:
        gaffile.write(gaf)
    return gaf

if __name__ == '__main__':
    # query in SQL language
    query1="""SELECT  project.id as project_id,
        project.name as project_name,
        SPLIT_PART(project.name,'__',3) as challenge,
        CASE
            WHEN SPLIT_PART(project.name,'__',3) LIKE '%(%' THEN SPLIT_PART(SPLIT_PART(project.name,'__',3),'(',1)
            WHEN SPLIT_PART(project.name,'__',3) LIKE '%[%' THEN SPLIT_PART(SPLIT_PART(project.name,'__',3),'[',1)
        END as target_gene,
        annotation.id as annotation_id,
        annotation.textvalue as ontology_term,
        date(event.time) as annotation_date
        FROM annotation, project, projectannotationlink, event
        WHERE annotation.id = projectannotationlink.child
        AND project.id = projectannotationlink.parent
        AND annotation.update_id = event.id
        AND discriminator LIKE '%tag%'
        AND annotation.description LIKE '%currated%'"""

    # sends query to database gets result and stores it in a list in return
    queryResult = geneList(query1)

    # specifies the name of c_elegans GeneIDs file
    geneIdFileName = "c_elegans.PRJNA13758.WS276.geneIDs.txt"
    # generates a dictionnary of gene name linked to gene ID
    geneId = dictGeneId(geneIdFileName)
    # generates a dictionnary of gene name linked to objecttype
    TypeCode = dictTypeId(geneIdFileName)
    # generates a dictionnary of gene name linked to synonym
    Synonym = dictSynonym(geneIdFileName)
    
    # loads the Cedre ontology terms and number as dictionnary: CedrePheno:00000 as a dictionnary
    cedreOntologyDict = {}
    with open ("ontologyTermsIds.txt", "r") as txtFile:
        for line in txtFile:
            (key, val) = re.split("'",line)[1], re.split("'",line)[3]
            cedreOntologyDict[key] = val
    
    # saves SQL query result in csv format as sqlExtract.csv
    sqlExtract="sqlExtract.csv"
    with open(sqlExtract, "w") as dbExtract:
        dbExtract.write(queryResultToCsv(queryResult))

    # creates a GAF2.0 formated file of phenotype associations from sqlExtract.csv
    csvToGAF2(sqlExtract, cedreOntologyDict)