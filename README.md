# StageM1

## Use of: gene_list_from_ls_xml.py
### To generate the .txt file to be used
In the appropriate rep run: ls *.xml >> lsfile.txt

### Command:
python3 gene_list_from_ls_xml.py lsfile.txt


## Use of: cedreOmeroSqlToGaf2.py
### Needs the files:
- database.ini
- c_elegans.PRJNA13758.WS276.geneIDs.txt
- ontologyTermsIds.txt

### Command:
./cedreOmeroSqlToGaf2.py

## Use of: CedreWbPhenotype2rdf.py
### Needs :
a gaf2.0 file with .wb extension
the .wb file may have this format: whatever.version.wb then version is appended during conversion to .ttl

### Command:
./CedreWbPhenotype2rdf.py file.wb 6239

(6239 stands for c.elegans taxonID)

## TO DO
- Handle Cedre RNAi reference parsing in cedreOmeroSqlToGaf2.py